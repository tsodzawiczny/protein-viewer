const PDB_URL = "/data/1tqn.pdb";
const APPREGIO_DATA_URL = "/data/1tqn.json";

// Fetch and parse data sources
export async function fetchData() {
  try {
    const pdbResponse = await fetch(PDB_URL);
    const pdbData = await pdbResponse.text();

    const appregioResponse = await fetch(APPREGIO_DATA_URL);
    const appregioData = await appregioResponse.json();

    return { pdbData, appregioData };
  } catch (e) {
    console.error("Failed to fetch data", e);
  }
}

// Extracts the the atom id (e.g. "CG") from the atom's pdbline.
export function extractAtomId(atom) {
  return atom.pdbline.match(/[^ ]+\s+\d+\s+([^ ]+)/)[1];
}

// Add atom indexes to the appregio data
export function addIndexesToAppregioData(appregioData, model) {
  const allAtoms = model.selectedAtoms({});
  const indexMap = {};
  for (const atom of allAtoms) {
    const { resi, index } = atom;
    const atomId = extractAtomId(atom);
    indexMap[resi] = indexMap[resi] || {};
    indexMap[resi][atomId] = index;
  }

  function addIndex(node) {
    node.index = indexMap[node.auth_seq_id][node.auth_atom_id];
  }

  for (const edge of appregioData) {
    addIndex(edge.bgn);
    addIndex(edge.end);
  }
}
