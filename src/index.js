import "./index.css";

import { addIndexesToAppregioData, fetchData } from "./data";
import { showAtomInteraction } from "./atom_interactions";

// TODO: could be using imports, requires webpack condig to expose jquery as $
const $ = window.$;
const $3Dmol = window.$3Dmol;

async function render() {
  const element = $("#container-01");
  const config = { defaultcolors: $3Dmol.rasmolElementColors };
  const viewer = $3Dmol.createViewer(element, config);

  const { pdbData, appregioData } = await fetchData();

  viewer.addModel(pdbData, "pdb"); /* load data */
  addIndexesToAppregioData(appregioData, viewer.getModel());

  showAtomInteraction(viewer, appregioData);

  viewer.setStyle({}, { stick: { color: "spectrum" } }); /* style all atoms */
  viewer.mapAtomProperties($3Dmol.applyPartialCharges);
  viewer.zoomTo();
  viewer.render();
}

render();
