import { extractAtomId } from "./data";

// Get human-readable atom description.
function getAtomLabel(atom) {
  const atomId = extractAtomId(atom);
  return `${atom.resn} ${atom.resi} ${atomId}`;
}

// Add label to an atom. Returns the label.
function addLabelToAtom(viewer, atom, label) {
  return viewer.addLabel(label, {
    // TODO: extract styling
    inFront: true,
    fontSize: 18,
    position: {
      x: atom.x,
      y: atom.y,
      z: atom.z,
    },
  });
}

// Replaces `end` and `bgn` in an edge.
function reverseEdge(edge) {
  return {
    ...edge,
    end: edge.bgn,
    bgn: edge.end,
  };
}

// Returns all edges from this atom.
function getConnectedAtoms(atom, appregioData) {
  const bgnEdges = appregioData.filter((edge) => edge.bgn.index === atom.index);
  const endEdges = appregioData.filter((edge) => edge.end.index === atom.index);

  // all normalized to have current atom as bgn
  const normalizedEdges = [...bgnEdges, ...endEdges.map(reverseEdge)];
  return normalizedEdges;
}

// Shows atom interactions based on appregio data.
export function showAtomInteraction(viewer, appregioData) {
  let labels = [];
  let clickedAtomIndex = null;
  function clearLabels(viewer) {
    for (const label of labels) {
      viewer.removeLabel(label);
    }
    labels = [];
  }

  function onClick(atom, viewer) {
    console.log("click", atom);
    clearLabels(viewer);

    if (clickedAtomIndex === atom.index) {
      clickedAtomIndex = null;
      // just clear the labels and return
      return;
    }
    clickedAtomIndex = atom.index;

    const edges = getConnectedAtoms(atom, appregioData);
    const label = addLabelToAtom(viewer, atom, `${getAtomLabel(atom)}`);
    const style = label.getStyle();
    style.backgroundColor = 0x66ccff;
    viewer.setLabelStyle(label, style);
    labels.push(label);

    for (const edge of edges) {
      const atom = viewer
        .getModel()
        .selectedAtoms({ index: edge.end.index })[0];
      const label = addLabelToAtom(
        viewer,
        atom,
        `${getAtomLabel(atom)} (${edge.contact.join(", ")})`
      );
      labels.push(label);
    }
  }

  // note: hover is not reliable, but clicking does trigger this event
  function onHover(atom, viewer) {}
  function onUnhover(atom, viewer) {}

  const allAtoms = viewer.getModel().selectedAtoms({});
  for (const atom of allAtoms) {
    atom.clickable = true;
    atom.hoverable = true;
    atom.callback = onClick;
    atom.hover_callback = onHover;
    atom.unhover_callback = onUnhover;
  }
}
